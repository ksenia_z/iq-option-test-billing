<?php

use Illuminate\Database\Migrations\Migration;

class AddTriggerRestrictOperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = <<<SQL
            CREATE FUNCTION prevent_operation()
                RETURNS TRIGGER AS $$
            BEGIN
                IF TG_OP = 'DELETE'
                THEN
                    RAISE EXCEPTION 'Delete is forbidden'
                    USING ERRCODE = 'check_violation';
                ELSIF TG_OP = 'UPDATE'
                    THEN
                        IF OLD.status != 'pending' OR
                           row_to_json(NEW) :: JSONB - 'updated_at' - 'status' !=
                           row_to_json(OLD) :: JSONB - 'updated_at' - 'status'
                        THEN
                            RAISE EXCEPTION 'Update is forbidden, failed rows: new: %, old: %', NEW :: TEXT, OLD :: TEXT
                            USING ERRCODE = 'check_violation';
                        END IF;
                END IF;
            
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;
SQL;
        DB::statement($query);

        $query = <<<SQL
            CREATE TRIGGER prevent_operation
                BEFORE UPDATE OR DELETE ON billing_operations
                FOR EACH ROW
                EXECUTE PROCEDURE prevent_operation();
SQL;
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP FUNCTION prevent_operation() CASCADE');
    }
}
