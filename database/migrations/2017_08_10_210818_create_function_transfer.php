<?php

use Illuminate\Database\Migrations\Migration;

class CreateFunctionTransfer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = <<<SQL
            CREATE OR REPLACE FUNCTION transfer(
                _sender_id   INT,
                _receiver_id INT,
                _amount      NUMERIC(15, 6)
            )
                RETURNS BIGINT
            AS $$
            DECLARE
                _operation_id BIGINT;
                _child_id     BIGINT;
            BEGIN
                LOOP
                    IF pg_try_advisory_xact_lock(('X' || md5('billing_operations' || _sender_id)) :: BIT(64) :: BIGINT)
                    THEN
            
                        INSERT INTO billing_operations (user_id, amount, sender_id, receiver_id, status, type)
                        VALUES (_receiver_id, _amount, _sender_id, NULL, 'approved', 'transfer')
                        RETURNING id
                            INTO _child_id;
                        INSERT INTO billing_operations (user_id, amount, sender_id, receiver_id, status, type, child_id)
                        VALUES (_sender_id, -_amount, NULL, _receiver_id, 'approved', 'transfer', _child_id)
                        RETURNING id
                            INTO _operation_id;
                        RETURN _operation_id;
            
                    END IF;
                END LOOP;
            END;
            $$
            LANGUAGE plpgsql VOLATILE;
SQL;
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP FUNCTION transfer(INT, INT, NUMERIC)');
    }
}
