<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillingOperations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billing_operations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->decimal('amount', 15, 6);
            $table->integer('sender_id')->nullable();
            $table->foreign('sender_id')->references('id')->on('users');
            $table->integer('receiver_id')->nullable();
            $table->foreign('receiver_id')->references('id')->on('users');
            $table->integer('child_id')->nullable();
            $table->foreign('child_id')->references('id')->on('billing_operations');
            $table->enum('status', ['pending', 'approved', 'cancelled']);
            $table->enum('type', ['accrual', 'withdrawal', 'transfer', 'fee']);
            $table->index(['user_id', 'status']);
            $table->timestamps();
        });

        $query = <<<SQL
         ALTER TABLE billing_operations
            ADD CONSTRAINT check_accrual
         CHECK (CASE WHEN type = 'accrual'
            THEN amount > 0
            ELSE TRUE
         END);
SQL;
        DB::statement($query);

        $query = <<<SQL
         ALTER TABLE billing_operations
            ADD CONSTRAINT check_transfer
         CHECK (CASE WHEN type = 'transfer'
            THEN receiver_id IS NOT NULL AND child_id IS NOT NULL AND amount < 0 OR sender_id IS NOT NULL AND amount > 0
            ELSE TRUE 
          END);
SQL;
        DB::statement($query);

        $query = <<<SQL
         ALTER TABLE billing_operations
            ADD CONSTRAINT check_withdrawal
         CHECK (CASE WHEN type = 'withdrawal'
            THEN child_id IS NOT NULL AND amount < 0
            ELSE TRUE
         END);
SQL;
        DB::statement($query);

        $query = <<<SQL
         ALTER TABLE billing_operations
            ADD CONSTRAINT check_fee
         CHECK (CASE WHEN type = 'fee'
            THEN amount < 0
            ELSE TRUE
         END);
SQL;
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billing_operations');
    }
}
