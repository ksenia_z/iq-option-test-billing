<?php

use Illuminate\Database\Migrations\Migration;

class CreateTriggerTimestamps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = <<<SQL
            CREATE FUNCTION set_created_at()
                RETURNS TRIGGER AS $$
            BEGIN
                NEW.created_at := now();
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;
SQL;
        DB::statement($query);

        $query = <<<SQL
            CREATE TRIGGER set_created_at
                BEFORE INSERT ON billing_operations
                FOR EACH ROW
                EXECUTE PROCEDURE set_created_at();
SQL;
        DB::statement($query);

        $query = <<<SQL
            CREATE FUNCTION set_updated_at()
                RETURNS TRIGGER AS $$
            BEGIN
                NEW.updated_at := now();
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;
SQL;
        DB::statement($query);

        $query = <<<SQL
            CREATE TRIGGER set_updated_at
                BEFORE UPDATE ON billing_operations
                FOR EACH ROW
                EXECUTE PROCEDURE set_updated_at();
SQL;
        DB::statement($query);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP FUNCTION set_created_at() CASCADE');
        DB::statement('DROP FUNCTION set_updated_at() CASCADE');
    }
}
