<?php

use Illuminate\Database\Migrations\Migration;

class CreateFunctionAccrue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = <<<SQL
            CREATE OR REPLACE FUNCTION accrue(
                _user_id INT,
                _amount  NUMERIC(15, 6)
            )
                RETURNS BIGINT
            AS $$
            DECLARE
                _operation_id BIGINT;
            BEGIN
                INSERT INTO billing_operations (user_id, amount, status, type)
                VALUES (_user_id, _amount, 'approved', 'accrual')
                RETURNING id
                    INTO _operation_id;
                RETURN _operation_id;
            END;
            $$
            LANGUAGE plpgsql VOLATILE;
SQL;
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP FUNCTION accrue(INT, NUMERIC)');
    }
}
