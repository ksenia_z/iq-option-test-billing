<?php

use Illuminate\Database\Migrations\Migration;

class CreateFunctionWithdraw extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = <<<SQL
            CREATE OR REPLACE FUNCTION withdraw(
                _user_id     INT,
                _amount      NUMERIC(15, 6),
                _fee_percent NUMERIC(4, 2)
            )
                RETURNS BIGINT
            AS $$
            DECLARE
                _fee_amount   NUMERIC(15, 6);
                _operation_id BIGINT;
                _child_id     BIGINT;
            BEGIN
                LOOP
                    IF pg_try_advisory_xact_lock(('X' || md5('billing_operations' || _user_id)) :: BIT(64) :: BIGINT)
                    THEN
            
                        _fee_amount := _amount * _fee_percent / 100;
            
                        INSERT INTO billing_operations (user_id, amount, status, type)
                        VALUES (_user_id, -_fee_amount, 'pending', 'fee')
                        RETURNING id
                            INTO _child_id;
            
                        INSERT INTO billing_operations (user_id, amount, status, type, child_id)
                        VALUES (_user_id, -_amount + _fee_amount, 'pending', 'withdrawal', _child_id)
                        RETURNING id
                            INTO _operation_id;
                        RETURN _operation_id;
            
                    END IF;
                END LOOP;
            END;
            $$
            LANGUAGE plpgsql VOLATILE;
SQL;
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP FUNCTION withdraw(INT, NUMERIC, NUMERIC)');
    }
}
