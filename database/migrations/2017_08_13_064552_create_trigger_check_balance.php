<?php

use Illuminate\Database\Migrations\Migration;

class CreateTriggerCheckBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = <<<SQL
            CREATE FUNCTION check_balance()
                RETURNS TRIGGER AS $$
            DECLARE
                _balance NUMERIC(15, 6);
            BEGIN
                SELECT balance(NEW.user_id) + coalesce(NEW.amount, 0)
                INTO _balance;
            
                IF _balance < 0 THEN
                    RAISE EXCEPTION 'User % balance cannot be lower than zero. Balance: %', NEW.user_id, _balance
                    USING ERRCODE = 'check_violation';
                END IF;
            
                RETURN NEW;
            END;
            $$ LANGUAGE plpgsql;
SQL;
        DB::statement($query);

        $query = <<<SQL
            CREATE TRIGGER check_balance
                BEFORE INSERT ON billing_operations
                FOR EACH ROW
                EXECUTE PROCEDURE check_balance();
SQL;
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP FUNCTION check_balance() CASCADE');
    }
}
