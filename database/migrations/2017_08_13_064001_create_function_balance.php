<?php

use Illuminate\Database\Migrations\Migration;

class CreateFunctionBalance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $query = <<<SQL
            CREATE OR REPLACE FUNCTION balance(
                _user_id INT
            )
                RETURNS NUMERIC(15, 6)
            AS $$
            DECLARE
                _balance NUMERIC(15, 6);
            BEGIN
                SELECT coalesce(sum(amount), 0)
                INTO _balance
                FROM billing_operations
                WHERE user_id = _user_id AND status != 'cancelled';
            
                RETURN _balance;
            END;
            $$
            LANGUAGE plpgsql VOLATILE;
SQL;
        DB::statement($query);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP FUNCTION balance(INT)');
    }
}
