<?php

namespace App\Http\Controllers;

use Illuminate\Http\{
    Request, Response
};

class WithdrawalController extends Controller
{
    use Helpers;

    public function add(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
            'amount' => ['required', 'numeric'],
        ]);
        $user = \App\User::find($request->input('user_id'));
        $this->check_balance($user, $request->input('amount'), $request);

        \Queue::push(new \App\Jobs\WithdrawalJob($user, $request->input('amount')));
        return response()->json('', Response::HTTP_OK);
    }

}
