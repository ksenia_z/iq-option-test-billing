<?php

namespace App\Http\Controllers;

use Illuminate\Http\{
    Request, Response
};

class AccrualController extends Controller
{

    public function add(Request $request)
    {
        $this->validate($request, [
            'user_id' => ['required', 'exists:users,id'],
            'amount' => ['required', 'numeric', 'min:0'],
        ]);
        $user = \App\User::find($request->input('user_id'));
        $user->accrue($request->input('amount'));

        return response()->json('', Response::HTTP_OK);
    }

}
