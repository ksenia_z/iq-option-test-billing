<?php

namespace App\Http\Controllers;

use Illuminate\Http\{
    Request, Response
};

class TransferController extends Controller
{
    use Helpers;

    public function add(Request $request)
    {
        $this->validate($request, [
            'receiver_id' => ['required', 'exists:users,id'],
            'user_id' => ['required', 'exists:users,id'],
            'amount' => ['required', 'numeric', 'min:0'],
        ]);
        $user = \App\User::find($request->input('user_id'));
        $receiver = \App\User::find($request->input('receiver_id'));
        $this->check_balance($user, $request->input('amount'), $request);

        \Queue::push(new \App\Jobs\TransferJob($user, $receiver, $request->input('amount')));
        return response()->json('', Response::HTTP_OK);
    }

}
