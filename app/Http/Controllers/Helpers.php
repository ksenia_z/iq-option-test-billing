<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

trait Helpers
{
    public function check_balance(\App\User $user, float $amount, Request $request)
    {
        $validator = \Validator::make($request->all(), []);
        $validator->after(function ($validator) use ($user, $amount) {
            if ($user->balance() - $amount < 0) {
                $validator->errors()->add('amount', 'User does not have enough money');
            }
        });
        if ($validator->fails()) {
            $this->throwValidationException($request, $validator);
        }
    }
}
