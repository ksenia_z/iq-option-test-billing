<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    const FEE_PERCENT = 2.5;

    public function balance(): float
    {
        return StoredProcedureProcessor::execute('balance', $this->id);
    }

    public function accrue(float $amount): BillingOperation
    {
        $billing_operation_id = StoredProcedureProcessor::execute('accrue', [$this->id, $amount]);
        return BillingOperation::find($billing_operation_id);
    }

    public function transfer(int $receiver_id, float $amount): BillingOperation
    {
        $billing_operation_id = StoredProcedureProcessor::execute('transfer', [$this->id, $receiver_id, $amount]);
        return BillingOperation::find($billing_operation_id);
    }

    public function withdraw(float $amount): BillingOperation
    {
        $billing_operation_id = StoredProcedureProcessor::execute(
            'withdraw', [$this->id, $amount, self::FEE_PERCENT]
        );
        return BillingOperation::find($billing_operation_id);
    }
}
