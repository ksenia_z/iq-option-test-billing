<?php

namespace App;

class StoredProcedureProcessor
{
    public static function execute(string $name, $parameters): string
    {
        $bindings = implode(array_fill(0, count($parameters), '?'), ',');

        $result = \DB::select("select $name($bindings)", array_wrap($parameters));
        return $result[0]->$name;
    }
}
