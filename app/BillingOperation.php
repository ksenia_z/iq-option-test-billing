<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingOperation extends Model
{

    public function user()
    {
        return $this->belongsTo("App\User");
    }

    public function child()
    {
        return $this->hasOne("App\BillingOperation", 'id', 'child_id');
    }

    public function approve()
    {
        $this->status = 'approved';
        $this->save();
    }

    public function cancel()
    {
        \DB::transaction(function () {
            $this->status = 'cancelled';
            $this->save();

            $this->child->status = 'cancelled';
            $this->child->save();
        });
    }
}
