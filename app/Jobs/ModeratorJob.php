<?php

namespace App\Jobs;

/**
 * Imitate moderation of withdrawal requests.
 */

class ModeratorJob extends Job
{
    private $billing_operation;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\BillingOperation $billing_operation)
    {
        $this->billing_operation = $billing_operation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (mt_rand(0, 1) == 0) {
            $this->billing_operation->approve();
        } else {
            $this->billing_operation->cancel();
        }
    }
}
