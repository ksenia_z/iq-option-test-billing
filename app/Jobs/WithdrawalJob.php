<?php

namespace App\Jobs;

use Carbon\Carbon;

class WithdrawalJob extends Job
{
    private $user;
    private $amount;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\User $user, float $amount)
    {
        $this->user = $user;
        $this->amount = $amount;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $billing_operation = $this->user->withdraw($this->amount);
        if ($billing_operation) {
            \Queue::push((new ModeratorJob($billing_operation))->delay(Carbon::now()->addMinutes(10)));
        }
    }
}
