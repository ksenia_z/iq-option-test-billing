<?php

namespace App\Jobs;

class TransferJob extends Job
{
    private $user;
    private $amount;
    private $receiver;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(\App\User $user, \App\User $receiver, float $amount)
    {
        $this->user = $user;
        $this->amount = $amount;
        $this->receiver = $receiver;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->user->transfer($this->receiver->id, $this->amount);
    }
}
