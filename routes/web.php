<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->post('accrue', ['middleware' => 'auth_token', 'uses' => 'AccrualController@add']);
$app->post('withdrawal', ['middleware' => 'auth_token', 'uses' => 'WithdrawalController@add']);
$app->post('transfer', ['middleware' => 'auth_token', 'uses' => 'TransferController@add']);
