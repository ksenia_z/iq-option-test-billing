<?php

use Laravel\Lumen\Testing\DatabaseMigrations;

class UserTest extends TestCase
{
    use DatabaseMigrations;

    public function testAccrual()
    {
        $user = factory(App\User::class)->create();
        $user->accrue(200);
        $this->assertEquals($user->balance(), 200);
    }

    public function testWithdrawal()
    {
        $user = factory(App\User::class)->create();
        $user->accrue(200);
        $user->withdraw(50);
        $this->assertEquals($user->balance(), 150);
    }

    public function testApproval()
    {
        $user = factory(App\User::class)->create();
        $user->accrue(200);
        $operation = $user->withdraw(50);
        $operation->approve();
        $this->assertEquals($user->balance(), 150);
    }

    public function testCancel()
    {
        $user = factory(App\User::class)->create();
        $user->accrue(200);
        $operation = $user->withdraw(50);
        $operation->cancel();
        $this->assertEquals($user->balance(), 200);
    }
}
