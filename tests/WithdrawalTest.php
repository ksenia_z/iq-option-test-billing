<?php

use Laravel\Lumen\Testing\DatabaseMigrations;

class WithdrawalTest extends TestCase
{
    use DatabaseMigrations;

    public function testSuccess()
    {
        $user = factory(App\User::class)->create();
        $user->accrue(200);
        $params = ['user_id' => $user->id, 'amount' => 100];
        $this->json('POST', '/withdrawal', $params, $this->headers())->assertResponseOk();
        $this->seeInDatabase('billing_operations', [
            'type' => 'fee', 'amount' => -2.5
        ]);
        $this->seeInDatabase('billing_operations', [
            'type' => 'withdrawal', 'amount' => -97.5
        ]);
    }

    public function testIfZeroBalance()
    {
        $user = factory(App\User::class)->create();
        $params = ['user_id' => $user->id, 'amount' => 100];
        $this->json('POST', '/withdrawal', $params, $this->headers())
            ->assertResponseStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertEquals($user->balance(), 0);
    }

    public function testIfNegativeAmount()
    {
        $user = factory(App\User::class)->create();
        $params = ['user_id' => $user->id, 'amount' => -100];
        $this->json('POST', '/accrue', $params, $this->headers())
            ->assertResponseStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertEquals($user->balance(), 0);
    }

    public function testIfNotExistingUser()
    {
        $params = ['user_id' => 1000, 'amount' => 100];
        $this->json('POST', '/withdrawal', $params, $this->headers())
            ->assertResponseStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testIfNoToken()
    {
        $user = factory(App\User::class)->create();
        $params = ['user_id' => $user->id, 'amount' => 100];
        $this->json('POST', '/accrue', $params)
            ->assertResponseStatus(\Illuminate\Http\Response::HTTP_UNAUTHORIZED);
        $this->assertEquals($user->balance(), 0);
    }

    public function testIfAmountIsNotANumber()
    {
        $user = factory(App\User::class)->create();
        $user->accrue(200);
        $params = ['user_id' => $user->id, 'amount' => 'not_a_number'];
        $this->json('POST', '/withdrawal', $params, $this->headers())
            ->assertResponseStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertEquals($user->balance(), 200);
    }

    private function headers(): array
    {
        return ['API-Token' => env('API_TOKEN')];
    }
}
