<?php

use Laravel\Lumen\Testing\DatabaseMigrations;

class TransferTest extends TestCase
{
    use DatabaseMigrations;

    public function testSuccess()
    {
        $user = factory(App\User::class)->create();
        $user->accrue(150);
        $receiver = factory(App\User::class)->create();
        $params = ['user_id' => $user->id, 'receiver_id' => $receiver->id, 'amount' => 100];
        $this->json('POST', '/transfer', $params, $this->headers())->assertResponseOk();

        $this->assertEquals($user->balance(), 50);
        $this->assertEquals($receiver->balance(), 100);
    }

    public function testIfZeroBalance()
    {
        $user = factory(App\User::class)->create();
        $receiver = factory(App\User::class)->create();
        $params = ['user_id' => $user->id, 'receiver_id' => $receiver->id, 'amount' => 100];
        $this->json('POST', '/transfer', $params, $this->headers())
            ->assertResponseStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertEquals($user->balance(), 0);
        $this->assertEquals($receiver->balance(), 0);
    }

    public function testIfUserDoesNotExist()
    {
        $receiver = factory(App\User::class)->create();
        $params = ['user_id' => 1000, 'receiver_id' => $receiver->id, 'amount' => 100];
        $this->json('POST', '/transfer', $params, $this->headers())
            ->assertResponseStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertEquals($receiver->balance(), 0);
    }

    public function testIfReceiverDoesNotExist()
    {
        $user = factory(App\User::class)->create();
        $params = ['user_id' => $user->id, 'receiver_id' => 1000, 'amount' => 100];
        $this->json('POST', '/transfer', $params, $this->headers())
            ->assertResponseStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);

        $this->assertEquals($user->balance(), 0);
    }

    public function testIfNegativeAmount()
    {
        $user = factory(App\User::class)->create();
        $receiver = factory(App\User::class)->create();
        $params = ['user_id' => 1000, 'receiver_id' => $receiver->id, 'amount' => -100];
        $this->json('POST', '/transfer', $params, $this->headers())
            ->assertResponseStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertEquals($user->balance(), 0);
        $this->assertEquals($receiver->balance(), 0);
    }

    public function testIfNoToken()
    {
        $user = factory(App\User::class)->create();
        $receiver = factory(App\User::class)->create();
        $params = ['user_id' => 1000, 'receiver_id' => $receiver->id, 'amount' => 100];
        $this->json('POST', '/transfer', $params)
            ->assertResponseStatus(\Illuminate\Http\Response::HTTP_UNAUTHORIZED);
        $this->assertEquals($user->balance(), 0);
        $this->assertEquals($receiver->balance(), 0);
    }

    public function testIfAmountIsNotANumber()
    {
        $user = factory(App\User::class)->create();
        $receiver = factory(App\User::class)->create();
        $params = ['user_id' => 1000, 'receiver_id' => $receiver->id, 'amount' => 'not_a_number'];
        $this->json('POST', '/transfer', $params, $this->headers())
            ->assertResponseStatus(\Illuminate\Http\Response::HTTP_UNPROCESSABLE_ENTITY);
        $this->assertEquals($user->balance(), 0);
    }

    private function headers(): array
    {
        return ['API-Token' => env('API_TOKEN')];
    }
}
